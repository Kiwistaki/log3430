/*
 * Nom du fichier: TestTP5.java
 * Auteurs: Carolyn Nguyen, Mai-Linh Nguyen et Alexandre Paquet
 * Description: Permet de tester toutes les fonctions de PileImpl.java 
 * Derniere modification: 02 avril 2017
 */
package testSuiteP;

import pile.PileImpl;
import static org.junit.Assert.*;

import java.util.EmptyStackException;

import org.junit.Test;

public class TestTP5 {
	
	//Premier cas de test : 
	//PileImpl,getSize, empiler, getsize, reset, getSize, depiler, getSize
	@Test(expected = EmptyStackException.class)
	public void test1() {
		int a = 1;
		PileImpl pile = new PileImpl();
		pile.getSize();
		pile.empiler(a);
		pile.getSize();
		pile.reset();
		int size1= pile.getSize();
		pile.depiler();
		int size2 = pile.getSize();
		assertEquals("size1 should be equal to size2 ", size1 = size2);
		
	}
	
	//Deuxieme cas de test: 
	//PileImpl,getSize, empiler, getsize, depiler, getSize, reset, getSize
	@Test
	public void test2() {
		int a= 2;
		PileImpl pile = new PileImpl();
		pile.getSize();
		pile.empiler(a);
		pile.getSize();
		pile.depiler();
		pile.getSize();
		pile.reset();
		pile.getSize();
		boolean estVide = pile.estVide();
		assertEquals("estVide should be true ", true, estVide);
		
	}
	
	//3e cas de test:
	//PileImpl,getSize, reset, getsize, depiler, getSize, empiler, getSIze
	@Test(expected = EmptyStackException.class)
	public void test3() {
		PileImpl pile = new PileImpl();
		pile.getSize();
		pile.reset();
		pile.getSize();
		pile.depiler();
		int size1 = pile.getSize();
		int a = 3;
		pile.empiler(a);
		int size2 = pile.getSize();
		assertTrue("size1 should be smaller than size2 ", size1 < size2);
	}
	
	//4e cas de test:
	//PileImpl,getSize, reset, getsize, empiler, getSize, depiler, getSIze
	@Test
	public void test4() {
		PileImpl pile = new PileImpl();
		pile.getSize();
		pile.reset();
		pile.getSize();
		int a = 3;
		pile.empiler(a);
		int size1 = pile.getSize();
		pile.depiler();
		int size2 = pile.getSize();
		assertTrue("size1 should be bigger than size2 ", size1 > size2);
	}
	
	//5e cas de test
	//PileImpl,getSize, depiler, getsize, reset, getSize, empiler, getSize
	@Test(expected = EmptyStackException.class)
	public void test5() {
		int a = 5;
		PileImpl pile = new PileImpl();
		pile.getSize();
		pile.depiler();
		pile.getSize();
		pile.reset();
		int size1 = pile.getSize();
		pile.empiler(a);
		int size2 = pile.getSize();
		assertTrue("size1 should be smaller than size2 ", size1 < size2);
	}
	
	//6e cas de test
	//PileImpl,getSize, depiler, getsize, empiler, getSize, reset, getSize
	@Test(expected = EmptyStackException.class)
	public void test6() {
		int a = 6;
		PileImpl pile = new PileImpl();
		pile.getSize();
		pile.depiler();
		pile.getSize();
		pile.empiler(a);
		int size1 = pile.getSize();
		pile.reset();
		int size2 = pile.getSize();
		assertEquals("size1 should be bigger than size2 ", size1 > size2);
	}
	
	//7e cas de test
	//PileImpl,getSize, empiler, getsize, getHead, estVide
	@Test
	public void test7() {
		int a = 7;
		PileImpl pile = new PileImpl();
		pile.getSize();
		pile.empiler(a);
		pile.getSize();
		pile.getHead();
		boolean vide = pile.estVide();
		assertEquals("PileImpl shouldn't be empty ", false, vide);
	}
	
	//Test blanche pour couvrir 100% la classe PileImpl
	//PileImpl,getSize, getHead, getsize
	@Test(expected = EmptyStackException.class)
	public void testBlanche() {
		PileImpl pile = new PileImpl();
		pile.getSize();
		pile.getHead();
		int size = pile.getSize();
		assertEquals("PileImpl should be empty ", 0, size);
	}

}

// Nom du fichier: testEC.java
// Auteurs: Carolyn Nguyen, Mai-Linh Nguyen et Alexandre Paquet
// Description: Jeu de tests pour le critere EC

package testSuiteP;

import static org.junit.Assert.*;

import java.io.PrintStream;

import org.junit.Test;

import home.SuiteP;
import home.SuitePImpl;

public class testEC {
	
	@Test
	//Operateur: add, 1er entier: pair positif, 2e entier: positif et taille max: 7
	public void testAddition() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", 2, 3, 7)");
		System.out.println("Resultat attendu:  Pile:2 3 5 8 13 21 34");
		System.out.print("Resultat obtenu:   ");		
		test.build("add", 2, 3, 7);
		
	}
	
	@Test
	//Operateur: soust, 1er entier: pair positif, 2e entier: negatif et taille max: 4
	public void testSoustraction() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"soust\", 2, -3, 4)");
		System.out.println("Resultat attendu:  Pile:2 -3 5 -8");
		System.out.print("Resultat obtenu:   ");	
		test.build("soust", 2, -3, 4);
	}
	
	@Test
	//Operateur: mult, 1er entier: pair negatif, 2e entier: negatif et taille max: 5
	public void testMultiplication() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"mult\", -4, -3, 5)");
		System.out.println("Resultat attendu:  Pile:-4 -3 -12 -36 -432");
		System.out.print("Resultat obtenu:   ");
		test.build("mult", -4, -3, 5);
		//Retourne une erreur
	}
	
	@Test
	//Operateur: div, 1er entier: pair negatif, 2e entier: positif et taille max: 4
	public void testDivision() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", -120, 3, 5)");
		System.out.println("Resultat attendu:  Pile:-120 3 -40 0");
		System.out.print("Resultat obtenu:   ");
		test.build("div", -120, 3, 4);
	}
	
	@Test
	//operateur: div, 1er entier: pair positif, 2e entier: 61 et taille max: 3 
	public void testDivisionParBEgale0(){
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", 2, 61, 4)");
		System.out.println("Resultat attendu:  ERREUR");
		System.out.print("Resultat obtenu:   ");
		test.build("div", 2, 61, 4);
	}
	
	@Test
	//operateur: div, 1er entier: pair positif, 2e entier: 3 et taille max: 3 
	public void testDivisionAPlusPetitQueB(){
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", 2, 3, 4)");
		System.out.println("Resultat attendu:  ERREUR");
		System.out.print("Resultat obtenu:   ");
		test.build("div", 2, 3, 4);
	}
	
	@Test
	//operateur: add, 1er entier: pair positif, 2e entier: impair positif et taille max: 11 
	public void testTaillePileSup10(){
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", 2, 3, 11)");
		System.out.println("Resultat attendu: taille chaine doit etre inferieur/egal a 10");
		System.out.print("Resultat obtenu:   ");
		test.build("add", 2, 3, 11);
	}
	
	@Test
	//operateur: addi, 1er entier: pair positif, 2e entier: impair positif et taille max: 4 
	public void testOperateurInvalide(){
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"addi\", 2, 3, 4)");
		System.out.println("Resultat attendu:  operateur addi non identifié");
		System.out.print("Resultat obtenu:   ");
		test.build("addi", 2, 3, 4);
	}
	
	@Test
	//operateur: add, 1er entier: impair positif, 2e entier: impair positif et taille max: 4 
	public void testPremierImpair(){
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", 3, 3, 4)");
		System.out.println("Resultat attendu:  ERREUR - premier parametre de suiteP doit etre paire");
		System.out.print("Resultat obtenu:   ");
		test.build("add", 3, 3, 4);
		//Il faut changer le message d'erreur dans le code etant donne que c'est vrmt le premier parametre de la suite qui doit etre pair, et non le second.
	}
	
	@Test
	//operateur: add, 1er entier: pair positif, 2e entier: pair positif et taille max: 4 
	public void testDeuxiemePair(){
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", 2, 4, 4)");
		System.out.println("Resultat attendu:  ERREUR - deuxieme parametre de suiteP doit etre impaire");
		System.out.print("Resultat obtenu:   ");
		test.build("add", 2, 4, 4);
	}


}

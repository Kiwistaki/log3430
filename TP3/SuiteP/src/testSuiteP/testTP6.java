// Nom du fichier: testTP6.java
// Auteurs: Carolyn Nguyen, Mai-Linh Nguyen et Alexandre Paquet
// Description: Tests effectues par ordre de niveau de test

package testSuiteP;

import static org.junit.Assert.*;

import java.util.EmptyStackException;

import org.junit.Test;

import org.junit.runners.MethodSorters;

import home.SuiteP;
import home.SuitePImpl;
import pile.PileImpl;

import org.junit.FixMethodOrder;
import org.junit.Test;

//Ceci permet deffectuer les tests selon un ordre alphabetique, et donc 
//de conserver l'ordre etabli selon les niveaux de test
@FixMethodOrder(MethodSorters.NAME_ASCENDING) 
public class testTP6 {

	@Test
	//Test SuitePImpl build
	public void test_1() {
		SuitePImpl test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", 8, 5, 7)");
		System.out.println("Resultat attendu:  Pile:8 5 13 18 31 49 80");
		System.out.print("Resultat obtenu:   ");
		test.build("add", 8, 5, 7);
	}
	
	@Test
	//Test SuitePImpl printPile
	public void test_2() {
		SuitePImpl test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", 8, 5, 7)");
		System.out.println("Resultat attendu:  Pile:8 5 13 18 31 49 80");
		System.out.print("Resultat obtenu:   ");
		test.build("add", 8, 5, 7);
	}
	
	@Test
	public void test_3a() {
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", 8, 5, 7)");
		System.out.println("Resultat attendu:  Pile:8 5 13 18 31 49 80");
		System.out.print("Resultat obtenu:   ");
		test.build("add", 8, 5, 7);
	}
	
	@Test
	//Operateur: add, 1er entier: pair positif, 2e entier: negatif et taille max: 3
	public void test_3b() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", 10, -3, 3)");
		System.out.println("Resultat attendu:  Pile:10 -3 7");
		System.out.print("Resultat obtenu:   ");
		test.build("add", 10, -3, 3);
	}
	
	@Test
	//Operateur: add, 1er entier: pair negatif, 2e entier: positif et taille max: 4
	public void test_3c() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\",-8, 5, 4)");
		System.out.println("Resultat attendu:  Pile:-8 5 -3 2");
		System.out.print("Resultat obtenu:   ");
		test.build("add", -8, 5, 4);
	}
	
	@Test
	//Operateur: add, 1er entier: pair negatif, 2e entier: negatif et taille max: 3
	public void test_3d() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", -6, -5, 3)");
		System.out.println("Resultat attendu:  Pile:-6 -5 -11 ");
		System.out.print("Resultat obtenu:   ");
		test.build("add", -6, -5, 3);
	}
	
	@Test
	public void test_4a() {
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", 300, 3, 4)");
		System.out.println("Resultat attendu:  Pile:300 3 100 0");
		System.out.print("Resultat obtenu:   ");
		test.build("div", 300, 3, 4);
	}
	
	@Test
	//Operateur: div, 1er entier: pair positif, 2e entier: negatif et taille max: 3
	public void test_4b() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", 100, -25, 3)");
		System.out.println("Resultat attendu:  Pile:100 -25 -4 ");
		System.out.print("Resultat obtenu:   ");
		test.build("div", 100, -25, 3);
	}
	
	@Test
	//Operateur: div, 1er entier: pair negatif, 2e entier: positif et taille max: 4
	public void test_4c() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", -100, 25, 4)");
		System.out.println("Resultat attendu:  Pile:-100 25 -4 -6");
		System.out.print("Resultat obtenu:   ");
		test.build("div", -100, 25, 4);
	}
	
	@Test
	//Operateur: div, 1er entier: pair negatif, 2e entier: negatif et taille max: 4
	public void test_4d() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", -100, -25, 4)");
		System.out.println("Resultat attendu:  Pile:-100 -25 4 -6");
		System.out.print("Resultat obtenu:   ");
		test.build("div", -100, -25, 4);
	}
	
	@Test
	//operateur: div, 1er entier: pair positif, 2e entier: impair et taille max: 4 
	public void test_4e(){
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", 4, 51, 4)");
		System.out.println("Resultat attendu:  ERREUR");
		System.out.print("Resultat obtenu:   ");
		test.build("div", 4, 51, 4);
	}
	
	@Test
	//operateur: div, 1er entier: pair positif, 2e entier: 3 et taille max: 4 
	public void test_4f(){
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", 2, 3, 4)");
		System.out.println("Resultat attendu:  Pile:2 3 1 3");
		System.out.print("Resultat obtenu:   ");
		test.build("div", 2, 3, 4);
	}
	
	@Test
	//Test de la fonction multiply de la classe Calculator : b>0
	//Operateur: mult, 1er entier: pair negatif, 2e entier: positif et taille max: 3
	public void test_5a() {
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"mult\", -20, 3, 3)");
		System.out.println("Resultat attendu:  Pile:-20 3 -60");
		System.out.print("Resultat obtenu:   ");
		test.build("mult", -20, 3, 3);
	}
	
	@Test
	//Test de la fonction multiply de la classe Calculator : b<0
	//Operateur: mult, 1er entier: pair positif, 2e entier: negatif et taille max: 4
	public void test_5b() {
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"mult\",  4, -3, 4)");
		System.out.println("Resultat attendu:  Pile:4 -3 -12 36");
		System.out.print("Resultat obtenu:   ");
		test.build("mult", 4, -3, 4);
	}
	
	@Test
	//Test de la fonction substract de la classe Calculator: b>0
	//Operateur: soust, 1er entier: pair positif, 2e entier: positif et taille max: 4
	public void test_6a() {
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"soust\", 20, 5, 4)");
		System.out.println("Resultat attendu:  Pile:20 5 15 -10");
		System.out.print("Resultat obtenu:   ");
		test.build("soust", 20, 5, 4);
	}
	
	@Test
	//Test de la fonction substract de la classe Calculator: b<0
	//Operateur: soust, 1er entier: pair positif, 2e entier: negatif et taille max: 4
	public void test_6b() {
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"soust\", 20, -5, 4)");
		System.out.println("Resultat attendu:  Pile:20 -5 25 -30");
		System.out.print("Resultat obtenu:   ");
		test.build("soust", 20, -5, 4);
	}
	
	@Test
	//Test de la fonction PileImpl de la classe PileImpl
	public void test_7() {
		int itemA= 1;
		PileImpl pile = new PileImpl();
		pile.empiler(itemA);
		assertEquals("Size of pile should be 1", 1, pile.getSize());
	}
	
	@Test
	//Test de la fonction depiler de la classe PileImpl si la pile contient des elements
	public void test_8a() {
		PileImpl pile = new PileImpl();
		int a = 1, b = 2;
		pile.empiler(a);
		pile.empiler(b);
		assertEquals("Size of pile should be 2", 2, pile.getSize());
		pile.depiler();
		assertEquals("Size of pile should be 1", 1, pile.getSize());
	}
	
	@Test(expected = EmptyStackException.class)
	//Test de la fonction depiler de la classe PileImpl si la pile est vide
	public void test_8b() {
		PileImpl pile = new PileImpl();
		pile.depiler();
	}
	
	@Test
	//Test de la methode empiler() de la classe PileImpl
	public void test_9() {
		int itemA= 1;
		PileImpl pile = new PileImpl();
		pile.empiler(itemA);
		assertEquals("Size of pile should be 1", 1, pile.getSize());
	}
	
	@Test
	//Test de la methode estVide() de la classe PileImpl
	public void test_10() {
		int itemA= 1;
		PileImpl pile = new PileImpl();
		pile.empiler(itemA);
		assertSame("estVide should return true", false, pile.estVide());
		pile.reset();
		assertSame("estVide should return true", true, pile.estVide());
	}
	@Test(expected = EmptyStackException.class)
	//Test de la methode getHead() de la classe PileImpl si pile ne contient pas d'element 
	public void test_11a() {
		PileImpl pile = new PileImpl();
		pile.getHead();
	}
	
	@Test
	//Test de la methode getHead() de la classe PileImpl si pile contient element
	public void test_11b() {
		int itemA= 1;
		int itemB= 2;
		PileImpl pile = new PileImpl();
		pile.empiler(itemA);
		pile.empiler(itemB);
		assertEquals("getHead should return 2", 2, pile.getHead());
	}
	
	@Test
	//Test de la methode getSize() de la classe PileImpl
	public void test_12() {
		PileImpl pile = new PileImpl();
		assertEquals("Size of pile should be 0", 0, pile.getSize());
		int a = 1;
		pile.empiler(a);
		pile.empiler(a);
		assertEquals("Size of pile should be 2", 2, pile.getSize());
	}
	@Test
	//Test de la methode reset() de la classe PileImpl
	public void test_13() {
		int itemA= 1;
		PileImpl pile = new PileImpl();
		pile.empiler(itemA);
		pile.reset();
		assertSame("estVide should return true", true, pile.estVide());
	}
	

}

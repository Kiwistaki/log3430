/*
 * Nom du fichier: TestPileImpl.java
 * Auteurs: Carolyn Nguyen, Mai-Linh Nguyen et Alexandre Paquet
 * Description: Permet de tester toutes les fonctions de PileImpl.java 
 * Derniere modification: 19 mars 2017
 */

package testSuiteP;

import pile.PileImpl;
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.EmptyStackException;

public class TestPileImpl {

	//1. Q0 -> Q1 -getHead()-> Q4 
	@Test(expected = EmptyStackException.class)
	public void test1(){
		PileImpl pile = new PileImpl();
		pile.getHead();
	}
	
	//2. Q0 -> Q1 -depiler()-> Q4
	@Test(expected = EmptyStackException.class)
	public void test2() {
		PileImpl pile = new PileImpl();
		pile.depiler();
	}
	
	//3. Q0 -> Q1 -empiler()-> Q2 -reset()-> Q1 -estVide()-> Q1
	@Test
	public void test3(){
		int itemA= 1;
		PileImpl pile = new PileImpl();
		pile.empiler(itemA);
		pile.reset();
		assertSame("estVide should return true", true, pile.estVide());
		
	}
	
	//4. Q0 -> Q1 -getSize()-> Q1 -empiler()-> Q2 -empiler()-> Q2 -estVide()-> Q2
	@Test
	public void test4(){
		PileImpl pile = new PileImpl();
		pile.getSize();
		assertEquals("Size of pile should be 0", 0, pile.getSize());
		int a = 1;
		pile.empiler(a);
		pile.empiler(a);
		assertSame("estVide should return false", false, pile.estVide());
	}
	
	//5. Q0 -> Q1 -getSize()-> Q1 -empiler()-> Q2 -empiler()-> Q2 -depiler()-> Q2 -delete-> Q3
	@Test
	public void test5(){
		PileImpl pile = new PileImpl();
		pile.getSize();
		assertEquals("Size of pile should be 0", 0, pile.getSize());
		int a = 1, b = 2;
		pile.empiler(a);
		pile.empiler(b);
		pile.depiler();
		assertEquals("Size of pile should be 1", 1, pile.getSize());
		pile = null; // on supprime la pile
		assertEquals("Pile should be null", null, pile);
	}
	
	//6. Q0 -> Q1 -empiler()-> Q2 -empiler()-> Q2 -getSize()-> Q2 -getHead()-> Q2
	@Test
	public void test6(){
		int itemA= 1;
		int itemB= 2;
		PileImpl pile = new PileImpl();
		pile.empiler(itemA);
		assertEquals("Size of pile should be 1", 1, pile.getSize());
		pile.empiler(itemB);
		assertEquals("Size of pile should be 2", 2, pile.getSize());
		assertEquals("getHead should return 2", 2, pile.getHead());
		
		
	}

}

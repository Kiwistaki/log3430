package testSuiteP;

import static org.junit.Assert.*;

import org.junit.Test;

import home.SuiteP;
import home.SuitePImpl;

public class testAC {

	@Test
	//Operateur: add, 1er entier: pair positif, 2e entier: positif et taille max: 7
	public void testAddition1() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", 8, 5, 7)");
		System.out.println("Resultat attendu:  Pile:8 5 13 18 31 49 80");
		System.out.print("Resultat obtenu:   ");
		test.build("add", 8, 5, 7);
	}
	
	@Test
	//Operateur: add, 1er entier: pair positif, 2e entier: negatif et taille max: 3
	public void testAddition2() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", 10, -3, 3)");
		System.out.println("Resultat attendu:  Pile:10 -3 7");
		System.out.print("Resultat obtenu:   ");
		test.build("add", 10, -3, 3);
	}
	
	@Test
	//Operateur: add, 1er entier: pair negatif, 2e entier: positif et taille max: 4
	public void testAddition3() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\",-8, 5, 4)");
		System.out.println("Resultat attendu:  Pile:-8 5 -3 2");
		System.out.print("Resultat obtenu:   ");
		test.build("add", -8, 5, 4);
	}
	
	@Test
	//Operateur: add, 1er entier: pair negatif, 2e entier: negatif et taille max: 3
	public void testAddition4() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", -6, -5, 3)");
		System.out.println("Resultat attendu:  Pile:-6 -5 -11 ");
		System.out.print("Resultat obtenu:   ");
		test.build("add", -6, -5, 3);
	}
	
	@Test
	//Operateur: soust, 1er entier: pair positif, 2e entier: positif et taille max: 4
	public void testSoustraction1() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"soust\", 20, 5, 4)");
		System.out.println("Resultat attendu:  Pile:20 5 15 -10");
		System.out.print("Resultat obtenu:   ");
		test.build("soust", 20, 5, 4);
	}
	
	@Test
	//Operateur: soust, 1er entier: pair positif, 2e entier: negatif et taille max: 4
	public void testSoustraction2() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"soust\", 20, -5, 4)");
		System.out.println("Resultat attendu:  Pile:20 -5 25 -30");
		System.out.print("Resultat obtenu:   ");
		test.build("soust", 20, -5, 4);
	}
	
	@Test
	//Operateur: soust, 1er entier: pair negatif, 2e entier: positif et taille max: 5
	public void testSoustraction3() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"soust\", -2, 5, 5)");
		System.out.println("Resultat attendu:  Pile:-2 5 -7 12 -19");
		System.out.print("Resultat obtenu:   ");
		test.build("soust", -2, 5, 5);
	}
	
	@Test
	//Operateur: soust, 1er entier: pair negatif, 2e entier: negatif et taille max: 4
	public void testSoustraction4() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"soust\", -4, -1, 4)");
		System.out.println("Resultat attendu:  Pile:-4 -1 -3 2");
		System.out.print("Resultat obtenu:   ");
		test.build("soust", -4, -1, 4);
	}
	
	@Test
	//Operateur: mult, 1er entier: pair positif, 2e entier: positif et taille max: 5
	public void testMultiplication1() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"mult\", 4, 1, 5)");
		System.out.println("Resultat attendu:  Pile:4 1 4 4 16");
		System.out.print("Resultat obtenu:   ");
		test.build("mult", 4, 1, 5);
	}
	
	@Test
	//Operateur: mult, 1er entier: pair positif, 2e entier: negatif et taille max: 4
	public void testMultiplication2() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"mult\",  4, -3, 4)");
		System.out.println("Resultat attendu:  Pile:4 -3 -12 36");
		System.out.print("Resultat obtenu:   ");
		test.build("mult", 4, -3, 4);
		//Retourne une erreur
	}
	
	@Test
	//Operateur: mult, 1er entier: pair negatif, 2e entier: positif et taille max: 3
	public void testMultiplication3() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"mult\", -20, 3, 3)");
		System.out.println("Resultat attendu:  Pile:-20 3 -60");
		System.out.print("Resultat obtenu:   ");
		test.build("mult", -20, 3, 3);
	}
	
	@Test
	//Operateur: mult, 1er entier: pair negatif, 2e entier: negatif et taille max: 5
	public void testMultiplication4() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"mult\", -4, -5, 5)");
		System.out.println("Resultat attendu:  Pile:-4 -5 20 -100 -2000");
		System.out.print("Resultat obtenu:   ");
		test.build("mult", -4, -5, 5);
		//Retourne une erreur
	}
	
	
	@Test
	//Operateur: div, 1er entier: pair positif, 2e entier: positif et taille max: 4
	public void testDivision1() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", 300, 3, 4)");
		System.out.println("Resultat attendu:  Pile:300 3 100 0");
		System.out.print("Resultat obtenu:   ");
		test.build("div", 300, 3, 4);
	}
	
	@Test
	//Operateur: div, 1er entier: pair positif, 2e entier: negatif et taille max: 3
	public void testDivision2() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", 100, -25, 3)");
		System.out.println("Resultat attendu:  Pile:100 -25 -4 ");
		System.out.print("Resultat obtenu:   ");
		test.build("div", 100, -25, 3);
	}
	
	@Test
	//Operateur: div, 1er entier: pair negatif, 2e entier: positif et taille max: 4
	public void testDivision3() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", -100, 25, 4)");
		System.out.println("Resultat attendu:  Pile:-100 25 -4 -6");
		System.out.print("Resultat obtenu:   ");
		test.build("div", -100, 25, 4);
	}
	
	@Test
	//Operateur: div, 1er entier: pair negatif, 2e entier: negatif et taille max: 4
	public void testDivision4() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", -100, -25, 4)");
		System.out.println("Resultat attendu:  Pile:-100 -25 4 -6");
		System.out.print("Resultat obtenu:   ");
		test.build("div", -100, -25, 4);
	}

}

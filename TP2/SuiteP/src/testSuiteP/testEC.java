package testSuiteP;

import static org.junit.Assert.*;

import java.io.PrintStream;

import org.junit.Test;

import home.SuiteP;
import home.SuitePImpl;

public class testEC {
	
	@Test
	//Operateur: add, 1er entier: pair positif, 2e entier: positif et taille max: 7
	public void testAddition() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"add\", 2, 3, 7)");
		System.out.println("Resultat attendu:  Pile:2 3 5 8 13 21 34");
		System.out.print("Resultat obtenu:   ");		
		test.build("add", 2, 3, 7);
		
	}
	
	@Test
	//Operateur: soust, 1er entier: pair positif, 2e entier: negatif et taille max: 4
	public void testSoustraction() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"soust\", 2, -3, 4)");
		System.out.println("Resultat attendu:  Pile:2 -3 5 -8");
		System.out.print("Resultat obtenu:   ");	
		test.build("soust", 2, -3, 4);
	}
	
	@Test
	//Operateur: mult, 1er entier: pair negatif, 2e entier: negatif et taille max: 5
	public void testMultiplication() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"mult\", -4, -3, 5)");
		System.out.println("Resultat attendu:  Pile:-4 -3 12 -36 -432");
		System.out.print("Resultat obtenu:   ");
		test.build("mult", -4, -3, 5);
		//Retourne une erreur
	}
	
	@Test
	//Operateur: div, 1er entier: pair negatif, 2e entier: positif et taille max: 4
	public void testDivision() {
		
		SuiteP test = new SuitePImpl();
		System.out.println("\n\nTest avec parametres (\"div\", -4, -3, 5)");
		System.out.println("Resultat attendu:  Pile:-120 3 -40 0");
		System.out.print("Resultat obtenu:   ");
		test.build("div", -120, 3, 4);
	}

}
